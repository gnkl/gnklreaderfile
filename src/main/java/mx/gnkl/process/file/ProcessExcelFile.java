/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.process.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author jmejia
 */
@Component
public class ProcessExcelFile {
    
    
    public List getBeanListFromXlsx(String className, String filepath) {
        List beanList = new ArrayList();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filepath);

            // Using XSSF for xlsx format, for xls use HSSF
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheetAt(0);
            int totalRows = sheet.getPhysicalNumberOfRows();
            Map<Integer, String> map = getHeaderFile((XSSFRow) sheet.getRow(0));
            //FormulaEvaluator objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) wb);
            FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
            DataFormatter objDefaultFormat = new DataFormatter();


            for (int x = 1; x < totalRows; x++) {
                XSSFRow dataRow = (XSSFRow) sheet.getRow(x);
                Iterator entries = map.entrySet().iterator();
                Class ftClass = Class.forName(className);
                Object bean = ftClass.newInstance();

                while (entries.hasNext()) {
                    try {
                        Map.Entry thisEntry = (Map.Entry) entries.next();
                        Integer key = (Integer) thisEntry.getKey();
                        String value = (String) thisEntry.getValue();
                        XSSFCell cell = dataRow.getCell(key);
                        
                        Field f1 = ftClass.getField(value.replace(" ", ""));
                        f1.set(bean, objDefaultFormat.formatCellValue(cell,objFormulaEvaluator));
                    } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException | NullPointerException ex) {
                        Logger.getLogger(ProcessGnklPunto.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                beanList.add(bean);
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        finally{
            if(fis!=null){
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(ProcessGnklPunto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return beanList;
    }
    
    
    private String remove2(String input) {
        String newInput = Normalizer.normalize(input, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        return newInput.replace(" ", "").trim().toLowerCase();
    }
    
   private Map<Integer, String> getHeaderFile(XSSFRow row){
        Map<Integer, String> map = new HashMap<Integer, String>(); //Create map
        short minColIx = row.getFirstCellNum(); //get the first column index for a row
        short maxColIx = row.getLastCellNum(); //get the last column index for a row                

        for (short colIx = minColIx; colIx < maxColIx; colIx++) { //loop from first to last index
            XSSFCell cell = row.getCell(colIx); //get the cell
            String cellName = remove2(cell.getStringCellValue());
            map.put(cell.getColumnIndex(), cellName);
        }       
        return map;
   }
   
    public List<Map<String, String>> getMapListFromXlsx(String filepath,Map<String, int[]> columns) throws FileNotFoundException, IOException {
        List<Map<String, String>> beanList = new ArrayList<Map<String, String>>();
        FileInputStream fis = null;
//        try {
            fis = new FileInputStream(filepath);

            // Using XSSF for xlsx format, for xls use HSSF
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheetAt(0);
            int totalRows = sheet.getPhysicalNumberOfRows();
            Map<Integer, String> map = getHeaderFile((XSSFRow) sheet.getRow(0));
            //FormulaEvaluator objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) wb);
            FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
            DataFormatter objDefaultFormat = new DataFormatter();


            for (int x = 1; x < totalRows; x++) {
                XSSFRow dataRow = (XSSFRow) sheet.getRow(x);
                Iterator entries = map.entrySet().iterator();
                //Class ftClass = Class.forName(className);
                //Object bean = ftClass.newInstance();
                if(dataRow!=null){
                    Iterator entriesColumn = columns.entrySet().iterator();
                    Map<String, String> item = new HashMap<String, String>();
                    while (entriesColumn.hasNext()) {
                      Entry thisEntry = (Entry) entriesColumn.next();
                      String key = (String) thisEntry.getKey();
                      int[] columnsNumber = (int[]) thisEntry.getValue();
                      String value="";
                      if(columnsNumber.length>0){
                          for(int i=0;i <columnsNumber.length;i++){
                             XSSFCell cell = dataRow.getCell(columnsNumber[i]);
                             if(cell!=null){
                                switch(cell.getCellType()){
                                    case Cell.CELL_TYPE_NUMERIC:
                                        value += String.valueOf(cell.getNumericCellValue());
                                        break;
                                    default:
                                        value += objDefaultFormat.formatCellValue(cell,objFormulaEvaluator)+" ";  
                                    break;
                                }
                                
                             }
                          }
                          item.put(key, value);   
                      }
                    } 
                    beanList.add(item);                    
                }
            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        finally{
            if(fis!=null){
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(ProcessGnklPunto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
//        }
        return beanList;
    }
    
    public List<Map<String, String>> getMapListFromXlsxAllString(String filepath,Map<String, int[]> columns) throws FileNotFoundException, IOException {
        List<Map<String, String>> beanList = new ArrayList<Map<String, String>>();
        FileInputStream fis = null;
//        try {
            fis = new FileInputStream(filepath);

            // Using XSSF for xlsx format, for xls use HSSF
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheetAt(0);
            int totalRows = sheet.getPhysicalNumberOfRows();
            Map<Integer, String> map = getHeaderFile((XSSFRow) sheet.getRow(0));
            //FormulaEvaluator objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) wb);
            FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
            DataFormatter objDefaultFormat = new DataFormatter();


            for (int x = 1; x < totalRows; x++) {
                XSSFRow dataRow = (XSSFRow) sheet.getRow(x);
                Iterator entries = map.entrySet().iterator();
                //Class ftClass = Class.forName(className);
                //Object bean = ftClass.newInstance();
                if(dataRow!=null){
                    Iterator entriesColumn = columns.entrySet().iterator();
                    Map<String, String> item = new HashMap<String, String>();
                    while (entriesColumn.hasNext()) {
                      Entry thisEntry = (Entry) entriesColumn.next();
                      String key = (String) thisEntry.getKey();
                      int[] columnsNumber = (int[]) thisEntry.getValue();
                      String value="";
                      if(columnsNumber.length>0){
                          for(int i=0;i <columnsNumber.length;i++){
                             XSSFCell cell = dataRow.getCell(columnsNumber[i]);
                             if(cell!=null){
                                switch(cell.getCellType()){
                                    case Cell.CELL_TYPE_NUMERIC:
                                        value += NumberToTextConverter.toText(cell.getNumericCellValue());
                                        break;
                                    default:
                                        value += objDefaultFormat.formatCellValue(cell,objFormulaEvaluator)+" ";  
                                    break;
                                }
                                
                             }
                          }
                          item.put(key, value);   
                      }
                    } 
                    beanList.add(item);                    
                }
            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        finally{
            if(fis!=null){
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(ProcessGnklPunto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
//        }
        return beanList;
    }    

    public List<Map<String, String>> getMapListFromXlsxAllString(String filepath,Integer sheetNum , Map<String, int[]> columns) throws FileNotFoundException, IOException {
        List<Map<String, String>> beanList = new ArrayList<Map<String, String>>();
        FileInputStream fis = null;
//        try {
            fis = new FileInputStream(filepath);

            // Using XSSF for xlsx format, for xls use HSSF
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheetAt(sheetNum);
            int totalRows = sheet.getPhysicalNumberOfRows();
            Map<Integer, String> map = getHeaderFile((XSSFRow) sheet.getRow(0));
            //FormulaEvaluator objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) wb);
            FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
            DataFormatter objDefaultFormat = new DataFormatter();


            for (int x = 1; x < totalRows; x++) {
                XSSFRow dataRow = (XSSFRow) sheet.getRow(x);
                Iterator entries = map.entrySet().iterator();
                //Class ftClass = Class.forName(className);
                //Object bean = ftClass.newInstance();
                if(dataRow!=null){
                    Iterator entriesColumn = columns.entrySet().iterator();
                    Map<String, String> item = new HashMap<String, String>();
                    while (entriesColumn.hasNext()) {
                      Entry thisEntry = (Entry) entriesColumn.next();
                      String key = (String) thisEntry.getKey();
                      int[] columnsNumber = (int[]) thisEntry.getValue();
                      String value="";
                      if(columnsNumber.length>0){
                          for(int i=0;i <columnsNumber.length;i++){
                             XSSFCell cell = dataRow.getCell(columnsNumber[i]);
                             if(cell!=null){
                                switch(cell.getCellType()){
                                    case Cell.CELL_TYPE_NUMERIC:
                                        value += NumberToTextConverter.toText(cell.getNumericCellValue());
                                        break;
                                    default:
                                        value += objDefaultFormat.formatCellValue(cell,objFormulaEvaluator)+" ";  
                                    break;
                                }
                                
                             }
                          }
                          item.put(key, value);   
                      }
                    } 
                    beanList.add(item);                    
                }
            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        finally{
            if(fis!=null){
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(ProcessGnklPunto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
//        }
        return beanList;
    }    
    
    
    public void writeCollection(String filePath, String sheetName, List<Map<String, Object>> collection, Map<Integer, String> header) throws Exception {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet(sheetName);

            int rows = sheet.getLastRowNum();        
            rows = rows + 1;
            //CREATE HEADER
            Map<Integer, String> headerObject = header;
            int columnHCount = 0;
            XSSFRow rowHeader = sheet.createRow(0);
            for (int i=0;i<headerObject.size();i++) {
                XSSFCell cell = rowHeader.createCell(i+1);
                cell.setCellValue(String.valueOf(headerObject.get(i)));
            }
            
            //CREATE BODY
            for (int index = 0; index < collection.size(); index++) {
                XSSFRow row = sheet.createRow(rows++);
                Map<String, Object> rowObject = collection.get(index);
                int columnCount = 0;
                for (String columnName : rowObject.keySet()) {
                    XSSFCell cell = row.createCell(++columnCount);
                    cell.setCellValue(String.valueOf(rowObject.get(columnName)));
                }
            }        

            try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
                workbook.write(outputStream);
            }   
    }


    public void writeCollectionMap(String filePath, String sheetName, List<Map<Integer, Object>> collection, Map<Integer, String> header) throws Exception {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet(sheetName);

            int rows = sheet.getLastRowNum();        
            rows = rows + 1;
            //CREATE HEADER
            Map<Integer, String> headerObject = header;
            int columnHCount = 0;
            XSSFRow rowHeader = sheet.createRow(0);
            for (int i=0;i<headerObject.size();i++) {
                XSSFCell cell = rowHeader.createCell(i+1);
                cell.setCellValue(String.valueOf(headerObject.get(i)));
            }
            
            //CREATE BODY
            for (int index = 0; index < collection.size(); index++) {
                XSSFRow row = sheet.createRow(rows++);
                Map<Integer, Object> rowObject = collection.get(index);
                int columnCount = 0;
                for (int j=0;j<rowObject.size();j++) {
                    XSSFCell cell = row.createCell(j+1);
                    cell.setCellValue(String.valueOf(rowObject.get(j)));
                }                
            }        

            try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
                workbook.write(outputStream);
            }   
    }
    
    public void writeList(String filePath, String sheetName, List<Map<Integer, List<Map<String,Object>>>> listTotal, Map<Integer, String> header) throws Exception {
            XSSFWorkbook workbook = new XSSFWorkbook();
            for(int k=0;k<listTotal.size();k++){
              Map<Integer, List<Map<String,Object>>> item = listTotal.get(k);
              List<Map<String, Object>> collection = item.get(k);
              XSSFSheet sheet = workbook.createSheet(sheetName+(k+1));
              
                int rows = sheet.getLastRowNum();        
                rows = rows + 1;
                //CREATE HEADER
                Map<Integer, String> headerObject = header;
                int columnHCount = 0;
                XSSFRow rowHeader = sheet.createRow(0);
                for (int i=0;i<headerObject.size();i++) {
                    XSSFCell cell = rowHeader.createCell(i+1);
                    cell.setCellValue(String.valueOf(headerObject.get(i)));
                }

                //CREATE BODY
                for (int index = 0; index < collection.size(); index++) {
                    XSSFRow row = sheet.createRow(rows++);
                    Map<String, Object> rowObject = collection.get(index);
                    int columnCount = 0;
                    for (String columnName : rowObject.keySet()) {
                        XSSFCell cell = row.createCell(++columnCount);
                        cell.setCellValue(String.valueOf(rowObject.get(columnName)));
                    }
                }                      
            }

            try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
                workbook.write(outputStream);
            }
    }    
    
    
}
