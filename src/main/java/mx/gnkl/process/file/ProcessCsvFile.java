/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.process.file;

import com.opencsv.CSVReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author jmejia
 */
@Component
public class ProcessCsvFile {
    
    public List getBeanListFromCsv(String className, String filepath) {

        List beanList = new ArrayList();

        CSVReader reader = null;
        try {

            reader = new CSVReader(new InputStreamReader(new FileInputStream(filepath), Charset.forName("ISO-8859-1")));
            String[] line;
            int x = 0;
            Map<Integer, String> header = getHeaderFile(reader.readNext());

            while ((line = reader.readNext()) != null) {
                Iterator entries = header.entrySet().iterator();
                try {

                    Class ftClass = Class.forName(className);
                    Object bean = ftClass.newInstance();

                    while (entries.hasNext()) {
                        try {
                            //Class ftClass = bean.getClass();
                            //System.out.println(ftClass);

                            Map.Entry thisEntry = (Map.Entry) entries.next();
                            Integer key = (Integer) thisEntry.getKey();
                            String value = (String) thisEntry.getValue();

                            Field f1 = ftClass.getField(value);
                            f1.set(bean, line[key]);

                        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException | NullPointerException ex) {
//                            Logger.getLogger(ProcessGnklPunto.class.getName()).log(Level.SEVERE, null, ex);
                            System.out.println(ex.getLocalizedMessage());
                            bean = null;
                        }
                    }
                    beanList.add(bean);

                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
//                    Logger.getLogger(ProcessGnklPunto.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println(ex.getLocalizedMessage());
                }
                x++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return beanList;
    }
    
   private Map<Integer, String> getHeaderFile(String[] row){
        Map<Integer, String> map = new HashMap<Integer, String>(); //Create map
        int minColIx = 0; //get the first column index for a row
        int maxColIx = row.length; //get the last column index for a row                

        for (int colIx = minColIx; colIx < maxColIx; colIx++) { //loop from first to last index
            String cell = row[colIx]; //get the cell
            String cellName = remove2(cell);
            map.put(colIx, cellName);
        }       
        return map;
   }
   
    private String remove2(String input) {
        String newInput = Normalizer.normalize(input, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        return newInput.replace(" ", "").trim().toLowerCase();
    }   
    
}
